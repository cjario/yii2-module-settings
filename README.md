Settings Module
===============
Settings module for Yii2

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist cj/yii2-module-settings "*"
```

or add

```
"cj/yii2-module-settings": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \cj\AutoloadExample::widget(); ?>```