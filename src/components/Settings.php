<?php
namespace cj\settings\components;

use cj\settings\models\Settings as SettingModel;
use yii\base\Component;
use yii\helpers\Html;

class Settings extends Component
{

    /**
     * return value for setting
     *
     * @method get
     * @param  string $slug
     * @return string
     * @author Tarun Mukherjee (https://github.com/tmukherjee13)
     */

    public static function get($slug)
    {

        $setting = SettingModel::findOne(['slug' => $slug]);
        if (!empty($setting)) {
            return $setting->getAttribute('value')??$setting->getAttribute('default');
        } else {
            return false;
        }

    }

    /**
     * Form Control
     *
     * Returns the form control for the setting.
     *
     * @todo: Code duplication, see modules/themes/controllers/admin.php @ form_control().
     *
     * @param   object  $setting
     * @return  string
     */
    public static function form_control(&$setting)
    {
        if ($setting->options) {

            // @usage func:function_name | func:helper/function_name | func:module/helper/function_name
            // @todo: document the usage of prefix "func:" to get dynamic options
            // @todo: document how construct functions to get here the expected data
            // if (substr($setting->options, 0, 5) == 'func:') {
            //     $func = substr($setting->options, 5);

            //     if (($pos = strrpos($func, '/')) !== false) {
            //         $helper  = substr($func, 0, $pos);
            //         $func    = substr($func, $pos + 1);

            //         if ($helper) {
            //             ci()->load->helper($helper);
            //         }
            //     }

            //     if (is_callable($func)) {
            //         // @todo: add support to use values scalar, bool and null correctly typed as params
            //         $setting->options = call_user_func($func);
            //     } else {
            //         $setting->options = array('=' . lang('global:select-none'));
            //     }
            // }

            // If its an array un-CSV it
            if (is_string($setting->options)) {
                $setting->options = explode('|', $setting->options);
            }
        }

        switch ($setting->type) {
            default:
            case 'text':

                $form_control = Html::textInput($setting->slug, $setting->value, $options = ['class' => 'form-control text width-20', 'id' => $setting->slug]);
                break;

            case 'textarea':
                $form_control = Html::textarea($setting->slug, $setting->value, $options = ['id' => $setting->slug, 'class' => 'form-control width-20']);
                break;

            case 'password':
                $form_control = Html::passwordInput($setting->slug, 'XXXXXXXXXXXX', $options = ['id' => $setting->slug, 'class' => 'form-control text width-20', 'autocomplete' => 'off']);
                break;

            case 'select':
                $form_control = Html::dropDownList($setting->slug, $setting->value, self::_format_options($setting->options), $options = ['class' => "form-control width-20"]);
                break;

            case 'select-multiple':
                $options      = self::_format_options($setting->options);
                $size         = sizeof($options) > 10 ? ' size="10"' : '';
                $form_control = form_multiselect($setting->slug . '[]', $options, explode(',', $setting->value), 'class="width-20"' . $size);
                break;

            case 'checkbox':

                $form_control  = '';
                $stored_values = is_string($setting->value) ? explode(',', $setting->value) : $setting->value;

                foreach (self::_format_options($setting->options) as $value => $label) {
                    if (is_array($stored_values)) {
                        $checked = in_array($value, $stored_values);
                    } else {
                        $checked = false;
                    }
                    $form_control .= Html::checkbox($setting->slug . '[]', $checked, $options = ['class' => '']);
                }

                break;

            case 'radio':

                $form_control = '';
                $form_control .= Html::radioList($setting->slug, $setting->value, self::_format_options($setting->options), []);
                break;
        }

        return $form_control;
    }

    /**
     * Format Options
     *
     * Formats the options for a setting into an associative array.
     *
     * @param   array   $options
     * @return  array
     */
    private static function _format_options($options = array())
    {
        $select_array = array();

        // echo "<pre>";
        // print_r($options);
        // echo "</pre>";

        foreach ($options as $option) {

            // var_dump($option);
            list($value, $key) = explode('=', $option);

            // if (ci()->lang->line('settings:form_option_' . $key) !== false) {
            //     $key = ci()->lang->line('settings:form_option_' . $key);
            // }
            // $key =  Yii::t('app', 'settings:form_option_' . $key);
            // $key =  Yii::t('app', 'settings:form_option_' . $key);
            $select_array[$value] = $key;
        }
        // echo "<pre>";
        // print_r($select_array);
        // echo "</pre>";
        // die;
        return $select_array;
    }
}
