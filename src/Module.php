<?php

namespace cj\settings;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'cj\settings\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
        \Yii::configure(\Yii::$app, require (__DIR__ . '/config/config.php'));
    }

    
}
