<?php
/* @var $this yii\web\View */
?>
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
	<div class="settings-form box box-primary">
        <?php 
        if ($setting_sections):
			$form = ActiveForm::begin([
			    'id' => 'login-form',
			    'options' => ['class' => ''],
			    'method' => 'post',
			    'action' => \Yii::$app->getUrlManager()->createUrl('settings-save'),
			    // 'action' => [Url::to('edit',true)],
			]) 
		 ?>
		<div class="row">
			<div class="tabs col-md-12">
                <div class="box-header">
    				<ul class="nav nav-tabs nav-justified">
    					<?php foreach ($setting_sections as $section_slug => $section_name): ?>
    					<li role="presentation" class="<?php echo array_search($section_name, array_values($setting_sections)) == 0 ? 'active' : null; ?>">
    						<a href="#<?php echo $section_slug ?>" data-toggle="tab" title="<?php printf(('settings:section_title'), $section_name) ?>">
    							<span><?php echo $section_name ?></span>
    						</a>
    					</li>
    					<?php endforeach ?>
    				</ul>
                </div>

				<div class="tab-content box-body">
    				<?php foreach ($setting_sections as $section_slug => $section_name): ?>
    					<div class="tab-pane <?php echo array_search($section_name, array_values($setting_sections)) == 0 ? 'active' : null; ?>" id="<?php echo $section_slug;?>">
    						<?php $section_count = 1; foreach ($settings[$section_slug] as $setting): ?>
    							<div class="form-group">
        							<div class="row">
        								
        								<label class="col-lg-5" for="<?php echo $setting->slug ?>">
        									<?php echo $setting->title ?>
        									<?php if($setting->description): ?>
        										<small class="help-block"><?php echo $setting->description; ?></small>
        									<?php endif; ?>
        								</label>

        								<div class="col-lg-3">
        									<?php echo $setting->form_control ?>
        								</div>
        							</div>  
    							</div>
    						<?php endforeach ?>
    					</div>
    				<?php endforeach ?>
				</div>
			</div>
		</div>
			
		<div class="box-footer">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
		</div>

		<?php ActiveForm::end() ?>

		<?php else: ?>
			<div>
				<p><?php echo Yii::t('app','No Settings Available');?></p>
			</div>
		<?php endif ?>

    </div>	
