<?php

namespace cj\settings\models;

use Yii;

/**
 * This is the model class for table "default_settings".
 *
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property string $type
 * @property string $default
 * @property string $value
 * @property string $options
 * @property integer $is_required
 * @property integer $is_gui
 * @property string $module
 * @property integer $order
 */
class Settings extends \yii\db\ActiveRecord
{

    public $form_control = '';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%default_settings}}';
    }

    public static function primaryKey()
    {
        return ['slug'];
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug', 'title', 'description'], 'required'],
            [['description', 'type', 'default', 'value'], 'string'],
            [['is_required', 'is_gui', 'order'], 'integer'],
            [['slug'], 'string', 'max' => 30],
            [['title'], 'string', 'max' => 100],
            [['options'], 'string', 'max' => 255],
            [['module'], 'string', 'max' => 50],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'slug'        => Yii::t('app', 'Slug'),
            'title'       => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'type'        => Yii::t('app', 'Type'),
            'default'     => Yii::t('app', 'Default'),
            'value'       => Yii::t('app', 'Value'),
            'options'     => Yii::t('app', 'Options'),
            'is_required' => Yii::t('app', 'Is Required'),
            'is_gui'      => Yii::t('app', 'Is Gui'),
            'module'      => Yii::t('app', 'Module'),
            'order'       => Yii::t('app', 'Order'),
        ];
    }

    /**
     * Get all settings that should be visible in the settings GUI.
     *
     * @method getGui
     * @return array
     * @author Tarun Mukherjee (https://github.com/tmukherjee13)
     */
    public static function getGui()
    {

        $settings = Settings::find()
            ->where('is_gui = :is_gui', [':is_gui' => 1])
            ->orderBy('order', 'ASC');

        // echo ($settings->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        return $settings->all();
    }

}
