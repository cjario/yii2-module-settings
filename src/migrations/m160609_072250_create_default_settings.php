<?php

use yii\db\Schema;
use yii\db\Migration;

class m160609_072250_create_default_settings extends Migration
{
      public function safeUp()
    {

        $this->createTable('{{%default_settings}}', array(
                'slug'=>"varchar(30) NOT NULL",
                'title'=>"varchar(100) NOT NULL",
                'description'=>"text NOT NULL",
                'type'=>"enum('text','textarea','password','select','select-multiple','radio','checkbox') NOT NULL DEFAULT 'text'",
                'default'=>"text DEFAULT NULL",
                'value'=>"text DEFAULT NULL",
                'options'=>"varchar(255) DEFAULT NULL",
                'is_required'=>"tinyint(1) NOT NULL",
                'is_gui'=>"tinyint(1) NOT NULL DEFAULT '1'",
                'module'=>"varchar(50) DEFAULT NULL",
                'order'=>"int(11) NOT NULL",
                'PRIMARY KEY (slug)' 

        ), ''); 
    }

    public function safeDown()
    {
        $this->dropTable('default_settings');
    }
}
