<?php
namespace cj\settings\controllers;

use tmukherjee13\core\controllers\CoreController;
use cj\settings\models\Settings;
use Yii;
use yii\helpers\Url;
use \GuzzleHttp\Client;

class SettingsController extends CoreController
{

    // public function behaviors()
    // {

    //     $behaviors = parent::behaviors();

    //     $behaviors['access'] = [
    //         'class' => AccessControl::className(),
    //         'only'  => ['create', 'update', 'index', 'delete', 'view'],
    //         'rules' => [
    //             [
    //                 'allow' => false,
    //                 'verbs' => ['POST'],
    //             ],
    //             [
    //                 'allow' => true,
    //                 'roles' => ['@'],
    //             ],
    //         ],
    //     ];

    //     $behaviors['verbs'] = [
    //         'class'   => VerbFilter::className(),
    //         'actions' => [
    //             'delete' => ['post'],
    //         ],
    //     ];

    //     return $behaviors;
    // }

    public function actionEdit()
    {
        $request   = Yii::$app->request;
        $post_data = $request->post();

        $settings        = Settings::getGui();
        $settings_stored = array();

        foreach ($settings as $setting) {
            $new_value = $request->post($setting->slug, false);

            // Store arrays as CSV
            if (is_array($new_value)) {
                $new_value = implode(',', $new_value);
            }

            // Only update passwords if not placeholder value
            if ($setting->type === 'password' and $new_value === 'XXXXXXXXXXXX') {
                continue;
            }

            // Dont update if its the same value
            if ($new_value != $setting->value) {

                $newSetting = Settings::findOne(["slug" => $setting->slug]);

                $newSetting->value = $new_value;
                $newSetting->save();

                $settings_stored[$setting->slug] = $new_value;
            }
        }

        Yii::$app->session->setFlash("success", "Settings Successfully updated");

        return $this->redirect(Url::to('settings'), 200);
    }

    public function actionIndex()
    {

        // Create a client with a base URI
        // $client = new Client(['base_uri' => API_URL]);

        // $response = $client->request('GET', 'settings');
        // $settings = $response->getBody();

        // $settings = json_decode($settings);
        // 
        $settings = Settings::getGui();

        $setting_language = array();
        $setting_sections = array();

        // Loop through each setting
        foreach ($settings as $key => $setting) {

            $setting->form_control = \cj\settings\components\Settings::form_control($setting);

            if (empty($setting->module)) {
                $setting->module = 'general';
            }

            if (!isset($setting_sections[$setting->module])) {
                $section_name = ucfirst(strtr($setting->module, '_', ' '));

                if (empty($section_name)) {
                    $section_name = ucfirst(strtr($setting->module, '_', ' '));
                }

                $setting_sections[$setting->module] = $section_name;
            }

            foreach (array('title' => $setting->slug, 'description' => $setting->slug . '_desc') as $key => $name) {

                // ${$key} = Yii::t('app',$name);
                ${$key} = Yii::t('app', $name);

                ${$key}          = Yii::t('app', $setting->{$key});
                $setting->{$key} = Yii::t('app', ${$key});
            }

            $settings[$setting->module][] = $setting;

            unset($settings[$key]);
        }

        return $this->render('index', compact('setting_sections', 'settings'));
    }
}
